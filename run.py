#!/usr/bin/env python3

from termcolor import colored
import subprocess
import argparse
import json
import time
import sys
import os

version = "v0.2"
debug = False
cur_dir = os.getcwd()
make_cross = os.path.join(cur_dir, "kbuild", "make.cross")
def_config = "config.json"
def_config_path = os.path.join(cur_dir, def_config)
compiler_dir = os.path.join(cur_dir, "compilers")
output_dir = os.path.join(cur_dir, "output")
configs_supported = [
    "allnoconfig", "allmodconfig", "alldefconfig", "allyesconfig"
]
kernel_configs_dir = os.path.join(cur_dir, "kernel-configs")
kernel_configs_supported = [filename for filename in os.listdir(kernel_configs_dir)]
configs_supported_all = configs_supported + kernel_configs_supported

stdout_file = "out.log"
stderr_file = "err.log"
linux_root_default = "~/git/linux"
n_cpus = os.cpu_count()
line_fmt = "%10s %28s    "
link_current_job = os.path.join(cur_dir, "current_job")
known_failures_config = os.path.join(cur_dir, "setup", "known-failures.json")
enforced_fixups_config = os.path.join(cur_dir, "setup", "enforced-fixups.json")

def load_config_file(path):
    return json.loads(open(path).read())

def load_known_failures(path):
    data = load_config_file(known_failures_config)
    # convert it into a hash so lookup is faster
    result = {}
    for entry in data:
        result[entry["arch"]] = {
            "configs": entry["configs"],
        }
    return result

known_failures = load_known_failures(known_failures_config)
enforced_fixups = load_config_file(enforced_fixups_config)

def get_sec():
    return time.time()

def set_env(name, value):
    os.environ[name] = value

def run_cmd_with_logs(cmd, out, err):
    out.write(colored("Running command: '%s'\n" % cmd, "yellow"))
    out.flush()
    subprocess.run(cmd, stdout=out, stderr=err, check=True)

def touch_dir(path):
    os.makedirs(path, exist_ok=True)

def dump_summary(tests):
    print()
    print(colored("Preparing to run below build tests:", "yellow"))
    print()
    for test in tests:
        print("  %10s with configs %s" % (test["arch"], test["configs"]))
    print()

def configs_full_check(configs):
    if "globals" not in configs:
        raise Exception("\"globals\" section is required in the config")
    if "tests" not in configs:
        raise Exception("\"tests\" section is required in the config")

def configs_load_list(configs):
    # We do accept "tests" only configs
    full = {
        "globals": {
            "linux-root": linux_root_default,
        },
        "tests": configs,
    }
    return full

def configs_fixups(all_configs):
    g_configs = all_configs["globals"]
    tests = all_configs["tests"]

    # Allow $HOME ("~") to be used
    linux_root = os.path.expanduser(g_configs["linux-root"])
    g_configs["linux-root"] = linux_root

    # Complete the "configs" option
    for test in tests:
        if "arch" not in test:
            raise Exception("'arch' is needed for all entries in 'tests'")
        configs = test["configs"]
        if isinstance(configs, str):
            if configs == "all":
                test["configs"] = configs_supported
            else:
                test["configs"] = [configs]
        for config in test["configs"]:
            if config not in configs_supported_all:
                raise Exception("Config '%s' not supported" % config)
    return all_configs

def configs_load(config_path):
    configs = open(config_path, "r").read()
    configs = json.loads(configs)

    if isinstance(configs, dict):
        configs_full_check(configs)
    elif isinstance(configs, list):
        configs = configs_load_list(configs)
    else:
        raise Exception("Unrecognized config file: " + config_path)

    configs = configs_fixups(configs)

    dump_summary(configs["tests"])
    return configs

def config_fixup_one(config, k, v, out, err):
    run_cmd_with_logs(["sed", "-i", "s/^%s=.*$/%s=%s/" % (k, k, v),
                       config], out, err)

def config_fixup(build_dir, g_configs, out, err):
    config = os.path.join(build_dir, ".config")

    # Apply global fixups first; they're known to be not working otherwise
    for k,v in enforced_fixups.items():
        config_fixup_one(config, k, v, out, err)

    # Skip the rest if nothing specified by the configs, or apply
    # customized fixups
    if "config-fixups" not in g_configs:
        return

    fixups = g_configs["config-fixups"]
    for k,v in fixups.items():
        config_fixup_one(config, k, v, out, err)

def test_supported(arch, config):
    if arch not in known_failures:
        return True
    configs = known_failures[arch]["configs"]
    if configs == "all":
        # All config will fail
        return False
    return config not in configs

def build_one(g_configs, arch, config):
    linux_root = g_configs["linux-root"]
    out_dir = os.path.join(output_dir, arch, config)
    build_dir = os.path.join(out_dir, "build")
    cmd_prefix = [make_cross, "O=" + build_dir, "ARCH=" + arch]

    # Create the root dir for this build
    touch_dir(build_dir)

    # Use stdout/stderr for the whole build process (of multiple cmds)
    out = open(os.path.join(out_dir, stdout_file), "w")
    err = open(os.path.join(out_dir, stderr_file), "w")

    # Always let current_job dir points to the working job
    if os.path.lexists(link_current_job):
        os.remove(link_current_job)
    os.symlink(out_dir, link_current_job)

    print(line_fmt % (arch, config), end='', flush=True)

    if not test_supported(arch, config):
        print(colored("SKIPPED", "yellow"))
        return

    # Switch to linux repo, and do the build test
    os.chdir(linux_root)

    time_start = get_sec()
    try:
        if debug:
            raise Exception("Skip due to debug")
        # Some arch builds requires this, I didn't check why, but it worked
        run_cmd_with_logs(["make", "ARCH=" + arch, "mrproper"], out, err)
        # Cleanup/prepare for the cross build world
        run_cmd_with_logs(cmd_prefix + ["mrproper"], out, err)

        # Prepare the kernel configurations
        if config in configs_supported:
            # "make XXXconfig" typed
            run_cmd_with_logs(cmd_prefix + [config], out, err)
        else:
            # copy the specified config file over
            run_cmd_with_logs(["cp", os.path.join(kernel_configs_dir, config),
                               os.path.join(build_dir, ".config")], out, err)
        # Fixup config under this build dir
        config_fixup(build_dir, g_configs, out, err)
        #
        # consolidate the config. this is needed when the fixup touched up
        # some higher level config, so this will further disable the
        # sub-configs underneath.
        #
        run_cmd_with_logs(cmd_prefix + ["olddefconfig"], out, err)

        # Go build!
        run_cmd_with_logs(cmd_prefix + ["-j%d" % n_cpus, "vmlinux"], out, err)
        print(colored("PASSED", "green"), end='')
    except Exception:
        print(colored("FAILED", "red"), end='')
    time_end = get_sec()
    print(" [time used: %.3f sec]" % (time_end - time_start))

    os.chdir(cur_dir)

def run_harness(configs):
    g_configs = configs["globals"]
    linux_root = g_configs["linux-root"]
    tests = configs["tests"]
    if not tests:
        raise Exception("Please specify \"tests\" first!")
    print(colored(line_fmt % ("ARCH", "CONFIG") + "RESULT", "yellow"))
    print("-" * 68)
    for entry in tests:
        arch = entry["arch"]
        configs = entry["configs"]
        for config in configs:
            build_one(g_configs, arch, config)
    print()

def prepare_harness(configs):
    set_env("COMPILER_INSTALL_PATH", compiler_dir)
    if "compiler" in configs["globals"]:
        gcc_version = configs["globals"]["compiler"]
        set_env("COMPILER", gcc_version)

def main():
    global debug

    parser = argparse.ArgumentParser(
        description="Linux kernel cross-build toolsets (%s)" % version)
    parser.add_argument("-c", "--config", type=str,
                        help="config files (*.json) to use (default: %s)"
                        % def_config, default=def_config_path)
    parser.add_argument("-d", "--debug", action="store_true",
                        help="run with debug mode", default=False)
    args = parser.parse_args()
    debug = args.debug
    configs = configs_load(args.config)
    prepare_harness(configs)
    run_harness(configs)

if __name__ == '__main__':
    main()
