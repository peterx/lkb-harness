.PYONY: clean all tail

all:
	@./run.py 2>&1 | tee summary.log

tail:
	@tail -f current_job/out.log

clean:
	@rm -rf ./output
	@rm -f current_job summary.log
