# lkb-harness

**lkb-harness** is a simple bunch of scripts to help do compilation tests
for any of your local Linux kernel branches.  Here **lkb** represents
**Linux Kernel Builds**.  It is based on the scripts provided by lkp-tests
projects to initiate the cross builds.

What it does is build-test the Linux branch that you're working with, on
any architecture or configurations specified.  It doesn't do any further
test with the built images.

## Installation

First, please make sure generic kernel build facilities are installed on
the host, so that it has the generic tools around.  Besides, some
pre-requisites are needed before a cross build (sample Fedora cmdlines):

``` bash
# Install termcolor package for Python3
$ sudo pip3 install termcolor

# Install "lftp" and "mkimage"
$ sudo dnf install lftp uboot-tools

# Clone this repository
$ git clone https://gitlab.com/peterx/lkb-harness.git
```

Then you're good to go!

Note that during the build tests, the test harness can download a bunch of
cross-compilers, and it will be put under *compilers* directory.

## Usage

The test harness is driven by a JSON configuration file, by default points
to *config.json* under the root test directory.  The default config can be
a good baseline to start, which should work for most recent Linux trees.
I'll try to keep it uptodate so that it will try to cover most of the
working archs/configs where the current default compilers should work.

What you normally need is to update the *linux-root* to point to your Linux
root directory, then kick off the build.

``` bash
# Setup linux-root directory
$ vim config.json

# Kick off all the builds.  Here "./run.py" will also work.
$ make
```

A sample output will look like this:

``` txt
Preparing to run below build tests:

      x86_64 with configs ['allnoconfig']
         arm with configs ['allnoconfig', 'alldefconfig']
       arm64 with configs ['allnoconfig']
     powerpc with configs ['allnoconfig']
          sh with configs ['allnoconfig']
       riscv with configs ['allnoconfig']
        m68k with configs ['allnoconfig']

      ARCH               CONFIG    RESULT
--------------------------------------------------------------------
    x86_64          allnoconfig    PASSED [time used: X.XXX sec]
       arm          allnoconfig    PASSED [time used: X.XXX sec]
       arm         alldefconfig    PASSED [time used: X.XXX sec]
     arm64          allnoconfig    PASSED [time used: X.XXX sec]
   powerpc          allnoconfig    PASSED [time used: X.XXX sec]
        sh          allnoconfig    PASSED [time used: X.XXX sec]
     riscv          allnoconfig    PASSED [time used: X.XXX sec]
      m68k          allnoconfig    PASSED [time used: X.XXX sec]
```

To specify a customized config file (which can include your own set of
builds to test), one can use:

``` bash
$ ./run.py -c $CONFIG_PATH
```

To look at the build progress of the current job when it's running:

``` bash
$ make tail
```

To cleanup build files (compilers will always be kept alone):

``` bash
$ make clean
```

One needs to manually cleanup the *compilers* directory, as by default
they'll always be cached to speed up future builds.

## Configuration File

An sample config file looks like:

``` JSON
{
    "globals": {
	"linux-root": "~/git/linux",
        "compilers": "gcc-10.5.0",
	"config-fixups": {
	    "CONFIG_SAMPLES": "n"
	}
    },
    "tests": [
	{
	    "arch": "x86_64",
	    "configs": ["allnoconfig", "allmodconfig"]
	}
    ]
}
```

Here:

- **globals**: this section describes global setups for all tests

  - **linux-root**: specifies the root directory of the Linux kernel source

  - **compiler**: specifies the compiler and versions for the cross-compile
    jobs.  Both gcc/clang should be supported.

  - **config-fixups**: we can provide customized setups globally for the
    build tests, like enable/disable some configs.  The major use case here
    is disable something that either will fail or you don't care in all
    setups. In this case, we disabled *CONFIG_SAMPLES* by default for all
    builds as it may not work well with cross builds. This will be applied
    on top of the configs we specified later for each test.

- **tests**: this holds all the build tests we want to do.  For each of the
  tests, we need:

  - **arch**: describes the arch of such test

  - **configs**: describes the base configs we want to build.  This can be
    either one config or a list of configs.  Currently only a few common
    configs (*allyesconfig*, *allnoconfig*, *alldefconfig*, *allmodconfig*)
    are supported.

There are quite a few other sample configuration files under *configs/*
directory.

## Issues

Feel free to leave me a message if you find an issue.
